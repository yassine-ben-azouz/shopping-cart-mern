import React, { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { getCartOwner } from "../../store/Slices/CartSlce";
import RemoveAllProductsInCart from "../Modals/RemoveAllProductsInCart";
import "./Basket.css";
import BasketCard from "./BasketCard";

const Basket = () => {
  const dispatch = useDispatch();

  // destructuring props
  // get totalAmount state from store

  const { cartList } = useSelector((state) => state.cart);
  useEffect(() => {
    dispatch(getCartOwner());
  }, []);

  const totalAmount = cartList.reduce(
    (acc, cur) => acc + cur.price * cur.quantity,
    0
  );

  const { toggleCart } = useSelector((state) => state.cart); // get toggleCart state from store
  return (
    <div>
      <Fragment>
        {/* check is toggleCart or not if is true show cart if is false hide cart (by change className name) */}
        <div className={toggleCart ? "col-4 basket" : "basket-none"}>
          <div className="basket-container">
            <h1>welcom to cart</h1>
            {cartList.length ? (
              <Fragment>
                <h3>you have {cartList.length} items in the cart</h3>

                <RemoveAllProductsInCart className="" />
                {cartList.map((item) => (
                  <BasketCard item={item} key={item._id} />
                ))}
                <Fragment>
                  <div className="checkout-total d-flex justify-content-between">
                    <h3>total : ${totalAmount.toFixed(2)}</h3>
                    <Link to="/order" className="btn btn-primary">
                      checkout
                    </Link>
                  </div>
                </Fragment>
              </Fragment>
            ) : (
              <h3>cart is empty</h3>
            )}
          </div>
        </div>
      </Fragment>
    </div>
  );
};

export default Basket;
