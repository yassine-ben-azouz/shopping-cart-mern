import React, { Fragment } from "react";
import { useDispatch } from "react-redux";
import {
  changeQantity,
  deletePrdoductInCart,
} from "../../store/Slices/CartSlce";

const BasketCard = (props) => {
  const { item } = props;
  const totalPrice = (item.quantity * item.price).toFixed(2); // declaration of totalPrice

  const dispatch = useDispatch();

  /*==== changeItemQantity ====*/
  const changeItemQantity = (choosenItem, payload) => {
    // check is quantity > 1 (|| qut ===1 for increment if qut postive)
    if (choosenItem.quantity > 1 || payload === 1) {
      dispatch(changeQantity({ choosenItem, payload }));
    }
  };
  /*====// changeItemQantity //====*/

  /*==== removeProduct ====*/
  const removeProduct = (id) => {
    dispatch(deletePrdoductInCart(id));
  };
  /*====// removeProduct //====*/

  return (
    <Fragment>
      <div className="basket-notification">
        <div className="content">
          <div className="basket-content">
            <img src={item.product.image} alt={item.product.title} />
            <div className="basket-body">
              <h5>{item.product.title}</h5>
              <div className="basket-action">
                <div className="basket-quantity">
                  <button
                    className="plus-quantity"
                    onClick={() => changeItemQantity(item, 1)}
                  >
                    +
                  </button>
                  <span>
                    quantity: <b>{item.quantity}</b>
                  </span>
                  <button
                    className="minus-qunatity"
                    onClick={() => changeItemQantity(item, -1)}
                  >
                    -
                  </button>
                  <span>
                    price: <b>${totalPrice}</b>
                  </span>
                </div>

                <button
                  className="remove-btn"
                  onClick={() => removeProduct(item.product._id)}
                >
                  remove
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default BasketCard;
