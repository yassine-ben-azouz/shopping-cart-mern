import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addInCart } from "../../../store/Slices/CartSlce";
import { deleteProduct } from "../../../store/Slices/ProductsSlice";
import EditProduct from "../../Admin/Edit Product/EditProduct";

/*==== ProductCard ====*/
const ProductCard = (props) => {
  const { product } = props;
  /*====// ProductCard //====*/
  const { isAuth, isAdmin } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  /*==== addCart ====*/
  function addCart(id) {
    dispatch(addInCart(id));
  }
  /*====// addCart //====*/
  return (
    <div className="col-lg-4 col-sm-6 card-product">
      <div className="product-card d-felx flex-column">
        <img
          className="product-card-img"
          src={product.image}
          alt={product.title}
        />
        <p className="product-card-title">{product.title}</p>
        <div className="product-card-action">
          <p className="price">{product.price} $</p>
          {isAdmin ? (
            <>
              <button
                className="btn btn-danger delete-btn"
                onClick={() => dispatch(deleteProduct(product._id))}
              >
                delete
              </button>
              <EditProduct id={product._id} />
            </>
          ) : (
            <button
              className={isAuth ? "add-btn" : "add-btn-display-none"}
              onClick={() => addCart(product._id)}
            >
              add to cart
            </button>
          )}

          <></>
        </div>

        {isAuth && (
          <Link className="see-more" to={`/product/${product._id}`}>
            see morre
          </Link>
        )}
      </div>
    </div>
  );
};

export default ProductCard;
