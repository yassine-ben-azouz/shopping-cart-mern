import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import Loader from "../../Loader/Loader";
import ProductCard from "./ProductCard";
const ProductsList = (props) => {
  const { loading, productsList } = useSelector((state) => state.products);

  return (
    <Fragment>
      {loading && <Loader />}

      <div className="products">
        <div className="col-8 d-flex flex-wrap ">
          {productsList.length ? (
            productsList.map((product) => (
              <ProductCard key={product._id} product={product} />
            ))
          ) : (
            <h1>product not found</h1>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default ProductsList;
