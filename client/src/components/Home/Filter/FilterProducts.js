import React, { Fragment } from "react";
import { useSelector } from "react-redux";

import "./FilterProducts.css";

const FilterProducts = (props) => {
  const { getSort, getSize, getSearchTilte } = props;
  const { countProducts } = useSelector((state) => state.products);

  //   dispatch(getSortProducts({ choosingSort }));
  // };
  // const searchTitle = (getSrarchTitle) => {
  //   dispatch(getSearchProduct({ getSrarchTitle }));
  // };
  return (
    <Fragment>
      <div className="col-9 d-flex filter">
        <div className="col-4">
          <input
            type="text"
            className="search-bar"
            placeholder=" search product by title..."
            onChange={(e) => getSearchTilte(e.target.value)}
          />
          {!!countProducts && (
            <div className="filer-result">count products {countProducts}</div>
          )}
        </div>
        <div className="col-4">
          <div className="filter-sort">
            sort
            <select onChange={(e) => getSort(e.target.value)}>
              <option value="lastes">lastes</option>
              <option value="lowest">lowest</option>
              <option value="highest">highest</option>
            </select>
          </div>
        </div>
        <div className="col-4">
          <div className="filter-size">
            filter
            <select onChange={(e) => getSize(e.target.value)}>
              <option value="" disabled>
                size
              </option>
              <option value="ALL">ALL</option>
              <option value="XS">XS</option>
              <option value="S">S</option>
              <option value="M">M</option>
              <option value="L">L</option>
              <option value="XL">XL</option>
              <option value="XXL">XXL</option>
            </select>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default FilterProducts;
