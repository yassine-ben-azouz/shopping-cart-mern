import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "./Pagination.css";
const Pagination = (props) => {
  const { getCurentPage } = props;
  const { pages } = useSelector((state) => state.products);
  const [currentPage, setCurrentPage] = useState(1);
  let generatedPages = [];
  for (let i = 1; i <= pages; i++) {
    generatedPages.push(i);
  }
  useEffect(() => {
    getCurentPage(currentPage);
  }, [currentPage]);
  return (
    <div>
      <div>
        <ul className="pagination">
          <button
            onClick={() => setCurrentPage((previousState) => previousState - 1)}
            disabled={currentPage === 1}
          >
            <i className="fa-solid fa-angle-left previous-page"></i>
          </button>
          {generatedPages.map((page) => (
            <li
              key={Math.random()}
              className={currentPage === page ? "page active-page" : "page "}
              onClick={() => setCurrentPage(page)}
            >
              {page}
            </li>
          ))}
          <button
            disabled={currentPage === pages}
            onClick={() => setCurrentPage((previousState) => previousState + 1)}
          >
            <i className="fa-solid fa-angle-right next-page"></i>
          </button>
        </ul>
      </div>
    </div>
  );
};

export default Pagination;
