import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useDispatch, useSelector } from "react-redux";
import { editProducts, getProducts } from "../../../store/Slices/ProductsSlice";

const EditProduct = ({ id }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [newProductData, setNewProductData] = useState({});
  const [imageUpload, setImageUpload] = useState({});
  const getDataInputs = (e) => {
    setNewProductData({ ...newProductData, [e.target.name]: e.target.value });
  };
  const dispatch = useDispatch();

  const updateProduct = (e) => {
    e.preventDefault();
    dispatch(editProducts({ ...newProductData, imageUpload, id }));
    handleClose();
  };
  return (
    <div>
      <>
        <Button className="edit-btn btn btn-primary" onClick={handleShow}>
          edit
        </Button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>update product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  className="form-input"
                  name="title"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">price</Form.Label>
                <Form.Control
                  className="form-input"
                  type="number"
                  placeholder="price..."
                  name="price"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">description</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="description"
                  name="description"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">category</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="write category"
                  name="availableSizes"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">photo</Form.Label>
                <Form.Control
                  className="form-input"
                  type="file"
                  placeholder="set image profil"
                  name="image"
                  onChange={(e) => setImageUpload(e.target.files[0])}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={(e) => updateProduct(e)}>
              update product
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    </div>
  );
};

export default EditProduct;
