import React from "react";
import "./GoBackButton.css";
import { useNavigate } from "react-router-dom";
const GoBackButton = () => {
  const navigate = useNavigate();
  const goBack = () => {
    navigate(-1);
  };
  return (
    <div>
      <button className="btn btn-secondary go-back-btn" onClick={goBack}>
        go back
      </button>
    </div>
  );
};

export default GoBackButton;
