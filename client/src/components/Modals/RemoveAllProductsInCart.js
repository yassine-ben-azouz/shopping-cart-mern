import React from "react";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import { deleteAllCartOwner } from "../../store/Slices/CartSlce";
const RemoveAllProductsInCart = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const dispatch = useDispatch();
  const removeAll = () => {
    dispatch(deleteAllCartOwner());
    handleClose();
  };
  return (
    <div className="container">
      <>
        <Button
          variant="danger"
          className="remove-all-btn"
          onClick={handleShow}
        >
          remove all
        </Button>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>
              {" "}
              are you sure to remove all products in cart{" "}
            </Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="danger" onClick={removeAll}>
              remove all products
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    </div>
  );
};

export default RemoveAllProductsInCart;
