import React from "react";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import { deleteAllCartOwner } from "../../store/Slices/CartSlce";
import { deleteOrder } from "../../store/Slices/OrderSlice";
const DeleteOrderUser = ({ id }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const dispatch = useDispatch();
  const removeOrder = () => {
    dispatch(deleteOrder(id));
    handleClose();
  };
  return (
    <div className="container">
      <>
        <Button variant="danger" className="delete-btn" onClick={handleShow}>
          delete
        </Button>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title> are you sure to delete order</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="danger" onClick={removeOrder}>
              delete order
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    </div>
  );
};

export default DeleteOrderUser;
