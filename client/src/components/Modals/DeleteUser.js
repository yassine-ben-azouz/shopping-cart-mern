import React from "react";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import { deleteUser } from "../../store/Slices/UsersSlice";
const DeleteUser = ({ id }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const dispatch = useDispatch();
  const removeUser = () => {
    dispatch(deleteUser(id));
    handleClose();
  };
  return (
    <div>
      {" "}
      <div className="container">
        <>
          <Button variant="danger" className="delete-btn" onClick={handleShow}>
            delete
          </Button>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title> are you sure to delete user</Modal.Title>
            </Modal.Header>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="danger" onClick={removeUser}>
                delete user
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      </div>
    </div>
  );
};

export default DeleteUser;
