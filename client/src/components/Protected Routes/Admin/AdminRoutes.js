import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
const AdminRoutes = () => {
  const { isAdmin } = useSelector((state) => state.users);
  console.log("isAdmin routes", isAdmin);
  return <div>{isAdmin ? <Outlet /> : <Navigate to="/login" />}</div>;
};

export default AdminRoutes;
