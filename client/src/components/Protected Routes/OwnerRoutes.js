import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";
const OwnerRoutes = () => {
  const { isAuth } = useSelector((state) => state.users);

  return (
    <Fragment> {isAuth ? <Outlet /> : <Navigate to="/login" />} </Fragment>
  );
};

export default OwnerRoutes;
