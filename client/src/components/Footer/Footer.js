import React from "react";
import { Link } from "react-router-dom";
import "./Footer.css";
const Footer = () => {
  return (
    <div>
      <div className="footer_section">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-sm-6">
              <h2 className="useful_text">Useful link </h2>
              <div className="footer_menu">
                <ul>
                  <li>
                    <Link to="#">Home</Link>
                  </li>
                  <li>
                    <Link to="#">About</Link>
                  </li>
                  <li>
                    <Link to="#">Our Designe</Link>
                  </li>
                  <li>
                    <Link to="#">Contact Us</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <h2 className="useful_text">Repair</h2>
              <p className="lorem_text">
                Lorem ipsum dolor sit amet, consectetur adipiscinaliqua
                Loreadipiscing{" "}
              </p>
            </div>
            <div className="col-lg-3 col-sm-6">
              <h2 className="useful_text">Social Media</h2>
              <div id="social">
                <Link className="facebookBtn smGlobalBtn active" to="#"></Link>
                <Link className="twitterBtn smGlobalBtn" to="#"></Link>
                <Link className="googleplusBtn smGlobalBtn" to="#"></Link>
                <Link className="linkedinBtn smGlobalBtn" to="#"></Link>
              </div>
            </div>
            <div className="col-sm-6 col-lg-3">
              <h1 className="useful_text">Our Repair center</h1>
              <p className="footer_text">
                Lorem ipsum dolor sit amet, consectetur
                adipiscinaliquaLoreadipiscing{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
