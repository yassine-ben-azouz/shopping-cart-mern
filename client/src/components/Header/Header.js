import React, { useEffect } from "react";
import "./Header.css";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { getUserInfo, logoutUser } from "../../store/Slices/UsersSlice";
import { toogleCartOwner } from "../../store/Slices/CartSlce";
const Header = () => {
  const { isAuth, isAdmin, userInfo } = useSelector((state) => state.users);
  const { cartList } = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  useEffect(() => {
    if (isAuth) {
      dispatch(getUserInfo());
    }
  }, []);
  return (
    <header>
      <div className="container p-3">
        <div className="row">
          <div className="col-4">
            <Link to="/" className="header-brand">
              shopping cart
            </Link>
          </div>
          <div className="col-8 d-flex align-items-center justify-content-end ">
            <nav className={isAuth ? "nav-links-isAuth" : "nav-links"}>
              {isAuth ? (
                <>
                  <Link to="/cart" className="link">
                    cart
                  </Link>

                  {isAdmin && (
                    <>
                      <Link to="/admin" className="link">
                        admin
                      </Link>
                    </>
                  )}

                  <div
                    className="cart-icon-notification"
                    // toogle cart notification (show and hide)
                    onClick={() => dispatch(toogleCartOwner())}
                  >
                    <i className="fa-solid fa-cart-plus cart-icon"></i>
                    <span className="badge">{cartList.length}</span>
                  </div>
                  <Link to={`/profil/${userInfo._id}`} className="link">
                    <img
                      src={userInfo.image}
                      alt={userInfo.username}
                      className="photo-profil"
                    />
                    {userInfo.username}
                  </Link>

                  <Link
                    to="/"
                    className="link"
                    onClick={() => dispatch(logoutUser())}
                  >
                    logout
                  </Link>
                </>
              ) : (
                <>
                  <Link to="/register" className="link">
                    rgister
                  </Link>
                  <Link to="/login" className="link">
                    login
                  </Link>
                </>
              )}
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
