import React, { Fragment } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import Home from "../pages/Home/Home";
import Register from "../pages/Register/Register";
import Login from "../pages/Login/Login";
import Profil from "../pages/Profil/Profil";
import Cart from "../pages/Cart/Cart";
import ProductDetails from "../pages/Product details/ProductDetails";

import Order from "../pages/Order/Order";
import OwnerRoutes from "../components/Protected Routes/OwnerRoutes";
import AdminRoutes from "../components/Protected Routes/Admin/AdminRoutes";
import Admin from "../pages/Admin/Admin";
import Users from "../pages/Admin/pages/Users/Users";
import Orders from "../pages/Admin/pages/Orders/Orders";

const Routers = () => {
  return (
    <Fragment>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/productDetails" element={<ProductDetails />} />

        <Route element={<OwnerRoutes />}>
          <Route path="/profil/" element={<Profil />} />
          <Route path="/profil/:id" element={<Profil />} />

          <Route path="/cart" element={<Cart />} />
          <Route path="/product/:id" element={<ProductDetails />} />
          <Route path="/order" element={<Order />} />
        </Route>
        <Route element={<AdminRoutes />}>
          <Route path="/admin/" element={<Admin />} />
          <Route path="/admin/register" element={<Register />} />
          <Route path="/admin/profil/:id" element={<Profil />} />
          <Route path="/admin/products/" element={<Home />} />
          <Route path="/admin/orders/" element={<Orders />} />
          <Route path="/admin/users" element={<Users />} />
        </Route>

        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    </Fragment>
  );
};

export default Routers;
