import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";
import { ToastContainer, toast, Slide } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Basket from "./components/Basket/Basket";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Routers from "./routes/Routers";

function App() {
  const { isAuth } = useSelector((state) => state.users);
  const { loading } = useSelector((state) => state.cart);

  return (
    <div className="App">
      <Header />
      {isAuth && <Basket />}
      <Routers />
      <Footer /> 
      <ToastContainer
        position={"bottom-right"}
        closeOnClick={true}
        transition={Slide}
        autoClose={"5000"}
        theme="colored"
        bodyStyle={{ color: "white" }}
      />
    </div>
  );
}

export default App;
