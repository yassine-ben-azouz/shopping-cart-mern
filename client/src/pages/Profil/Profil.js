import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import SideBar from "../../components/Admin/SideBar";
import GoBackButton from "../../components/Go-Back/GoBackButton";
import { getAllUsers, getUserInfo } from "../../store/Slices/UsersSlice";
import "./Profil.css";
const Profil = () => {
  const dispatch = useDispatch();

  const params = useParams();
  const userId = params.id;
  const { usersList, userInfo, isAdmin } = useSelector((state) => state.users);

  const userFind = usersList.find((user) => user._id === userId);
  const user = userFind || userInfo;
  useEffect(() => {
    dispatch(getUserInfo());
    if (isAdmin) {
      dispatch(getAllUsers());
    }
  }, []);
  return (
    <div className="row">
      {isAdmin && <SideBar />}
      <div
        className={isAdmin ? " container-profil-admin" : "container-profil "}
      >
        {" "}
        <div className="wrapper">
          <div className="left">
            <img src={user?.image} alt="user" width="100" />
            <h4>{user?.username}</h4>
            <p>UI Developer</p>
          </div>
          <div className="right">
            <div className="info">
              <h3>Information</h3>
              <div className="info_data">
                <div className="data">
                  <h4>Email</h4>
                  <p>{user?.email}</p>
                </div>
                <div className="data">
                  <h4>Phone</h4>
                  <p>0001-213-998761</p>
                </div>
              </div>
            </div>

            <div className="projects">
              <h3>Projects</h3>
              <div className="projects_data">
                <div className="data">
                  <h4>Recent</h4>
                  <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <div className="data">
                  <h4>Most Viewed</h4>
                  <p>dolor sit amet.</p>
                </div>
              </div>
            </div>

            <div className="social_media">
              <ul>
                <li>
                  <Link to="#">
                    <i className="fab fa-facebook-f"></i>
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i className="fab fa-twitter"></i>
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i className="fab fa-instagram"></i>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <GoBackButton />
    </div>
  );
};

export default Profil;
