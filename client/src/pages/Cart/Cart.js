import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader/Loader";
import {
  deletePrdoductInCart,
  getCartOwner,
} from "../../store/Slices/CartSlce";
const Cart = (props) => {
  // destructuring props
  // declartion dispatch for use useDispatch function
  const dispatch = useDispatch();
  /*==== removeProduct =====*/
  const removeProduct = (id) => {
    dispatch(deletePrdoductInCart(id));
  };
  /*====// removeProduct //=====*/

  useEffect(() => {
    dispatch(getCartOwner());
  }, []);
  const { cartList, loading } = useSelector((state) => state.cart);
  console.log(cartList);
  /*===== go back ====*/
  const navigate = useNavigate();
  function goBack() {
    navigate(-1);
  }
  return (
    <div>
      <div className=" col-8 card-item">
        {loading && <Loader />}
        {cartList.length ? (
          cartList.map((item) => (
            <div className="col-lg-3 col-sm-6 " key={item.product._id}>
              <div className="product-card d-felx flex-column">
                <img
                  className="product-card-img"
                  src={item.product.image}
                  alt={item.product.title}
                />
                <p>{item.product.title}</p>
                <div className=" product-card-action">
                  <p className="price">{item.product.price} $</p>
                  <button
                    className="delete-btn btn btn-danger"
                    onClick={() => removeProduct(item.product._id)}
                  >
                    delete item
                  </button>
                </div>
              </div>
            </div>
          ))
        ) : (
          <h1>Cart is empty</h1>
        )}
      </div>
      <button onClick={goBack} className="go-back-btn btn btn-secondary">
        go back
      </button>
    </div>
  );
};

export default Cart;
