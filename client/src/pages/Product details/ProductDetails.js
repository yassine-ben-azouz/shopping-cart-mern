import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import GoBackButton from "../../components/Go-Back/GoBackButton";
import { getProducts } from "../../store/Slices/ProductsSlice";
import "./ProductDetail.css";
const ProductDetail = () => {
  const { productsList } = useSelector((state) => state.products);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProducts());
  }, []);
  const params = useParams(); // get params of path params
  const choosenItemId = params.id;
  const choosenItem = productsList.find((item) => item._id === choosenItemId); // === because id is sting
  // map availableSizes products to show in product detail
  const availableSizes = choosenItem.availableSizes
    .filter((size) => size !== "ALL") // fillter by delete size all for not show in Card of choosenItem
    .map((size) => (
      <b className="size" key={Math.random()}>
        {size}
      </b>
    ));

  return (
    <div className="row">
      <div className="col-7">
        <div className="product-detail">
          <div className="product-detail-card col-6">
            <h3>{choosenItem.title}</h3>
            <img
              className="product-detail-image"
              src={choosenItem.image}
              alt={choosenItem.title}
            />
            <p className="product-detail-description">
              {choosenItem.description}
            </p>
            <div className="product-detail-size">{availableSizes}</div>
          </div>
          <GoBackButton />
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
