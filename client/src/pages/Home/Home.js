import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SiderBar from "../../components/Admin/SideBar";
import FilterProducts from "../../components/Home/Filter/FilterProducts";
import Pagination from "../../components/Home/Pagination/Pagination";

import ProductsList from "../../components/Home/Products/ProductsList";
import { getProducts } from "../../store/Slices/ProductsSlice";

const Home = (props) => {
  const [sort, setSort] = useState("lastes");
  const [size, setSize] = useState("ALL");
  const [title, setTitle] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const dispatch = useDispatch();
  // get products from database when sort, size or title value change
  useEffect(() => {
    dispatch(getProducts({ sort, size, title, currentPage }));
  }, [sort, size, title, currentPage]);

  // sort
  const getSort = (sortValue) => {
    setSort(sortValue);
  };
  // filter size
  const getSize = (sizeValue) => {
    setSize(sizeValue);
  };
  // filter seardh by title
  const getSearchTilte = (TitleValue) => {
    setTitle(TitleValue);
  };

  const getCurentPage = (currPage) => {
    setCurrentPage(currPage);
  };
  const { isAdmin } = useSelector((state) => state.users);
  // paggination
  return (
    <Fragment>
      <div className="container">
        <div className="row">
          <div className="container-all">
            <FilterProducts
              getSort={getSort}
              getSize={getSize}
              getSearchTilte={getSearchTilte}
            />
            <ProductsList />
          </div>
        </div>
      </div>
      <Pagination getCurentPage={getCurentPage} />
    </Fragment>
  );
};

export default Home;
