import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "./Register.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { registerUser } from "../../store/Slices/UsersSlice";
import SideBar from "../../components/Admin/SideBar";
const Register = () => {
  // declartion state for save data inputs login
  const [registerInputs, setRegisterInputs] = useState({});
  const [imageUpload, setImageUpload] = useState({});

  // get errors from store (state) to show errors message
  // get isAuth from store (state)  to check if is true to navigate to profil page
  const { errors, isAuth, registerAdmin, isAdmin } = useSelector(
    (state) => state.users
  );

  // useDispatch()
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // useNavigate()
  /*===  getDataInputs ====*/ // get date from inpust and save him into state
  function getDataInputs(e) {
    setRegisterInputs({
      ...registerInputs,

      [e.target.name]: e.target.value.trim(),
    });
  }
  /*===//  getDataInputs //====*/
  /*===  register ====*/
  function register(e) {
    e.preventDefault();
    dispatch(registerUser({ ...registerInputs, imageUpload }));
  }
  /*===//  register //====*/

  // useEffect fro check if isAuth true or note to navigate to profil page if is true
  useEffect(() => {
    if (!isAdmin) {
      isAuth && navigate("/profil");
    }
  }, [isAuth]);
  console.log("isAdmin", isAdmin);
  return (
    <div>
      <div className="row">
        {isAdmin && <SideBar />}
        <div className={isAdmin ? "col-8 " : ""}>
          <div className="form-container">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">username</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter username"
                  className="form-input"
                  name="username"
                  onChange={(e) => getDataInputs(e)}
                />
                <Form.Text className="text-muted">
                  {/* We'll never share your email with anyone else. */}
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">email address</Form.Label>
                <Form.Control
                  className="form-input"
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  onChange={(e) => getDataInputs(e)}
                />
                <Form.Text className="text-message">
                  {errors && <p>{errors}</p>}
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">password</Form.Label>
                <Form.Control
                  className="form-input"
                  type="password"
                  placeholder="Password"
                  name="password"
                  onChange={(e) => getDataInputs(e)}
                />
                <Form.Text className="text-message ">
                  {errors && <p>{errors}</p>}
                </Form.Text>
              </Form.Group>
              {isAdmin && (
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label className="text-label">role</Form.Label>
                  <Form.Control
                    className="form-input"
                    type="text"
                    placeholder="role"
                    name="role"
                    onChange={(e) => getDataInputs(e)}
                  />
                  <Form.Text className="text-message ">
                    {errors && <p>{errors}</p>}
                  </Form.Text>
                </Form.Group>
              )}

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">photo</Form.Label>
                <Form.Control
                  className="form-input"
                  type="file"
                  placeholder="set image profil"
                  name="image"
                  onChange={(e) => setImageUpload(e.target.files[0])}
                />
              </Form.Group>
              <Button
                variant=""
                type="submit"
                className="btn-submit"
                onClick={(e) => register(e)}
              >
                sing up
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
