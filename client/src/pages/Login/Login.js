import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginUser } from "../../store/Slices/UsersSlice";

const Login = () => {
  // declartion state for save data inputs login
  const [registerInputs, setRegisterInputs] = useState("");
  // useDispatch
  const dispatch = useDispatch();
  // useNavigate
  const navigate = useNavigate();
  /*===  getDataInputs ====*/ // get date from inpust and save him into state
  function getDataInputs(e) {
    setRegisterInputs({ ...registerInputs, [e.target.name]: e.target.value });
  }
  /*===//  getDataInputs //====*/

  // get errors from store (state) to show errors message
  // get isAuth from store (state)  to check if is true to navigate to profil page
  const { errors, isAuth } = useSelector((state) => state.users);

  // useEffect fro check if isAuth true or note to navigate to profil page if is true
  useEffect(() => {
    isAuth && navigate("/profil");
  }, [isAuth]);

  /*===  login ====*/
  function login(e) {
    e.preventDefault();
    dispatch(loginUser(registerInputs));
  }
  /*===//  login //====*/
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="form-container">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">email address</Form.Label>
                <Form.Control
                  className="form-input"
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  onChange={(e) => getDataInputs(e)}
                />
                <Form.Text className="text-message">
                  {errors?.includes("register") ? (
                    <p>{errors}</p>
                  ) : (
                    errors?.includes("fields") && <p>{errors}</p>
                  )}
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">password</Form.Label>
                <Form.Control
                  className="form-input"
                  type="password"
                  placeholder="Password"
                  name="password"
                  onChange={(e) => getDataInputs(e)}
                />
                <Form.Text className="text-message ">
                  {errors?.includes("password") ? (
                    <p>{errors}</p>
                  ) : (
                    errors?.includes("fields") && <p>{errors}</p>
                  )}
                </Form.Text>
              </Form.Group>

              <Button
                variant=""
                type="submit"
                className="btn-submit"
                onClick={(e) => login(e)}
              >
                login
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
