import React from "react";
import MainContent from "../../components/Admin/MainContent";
import SiderBar from "../../components/Admin/SideBar";

const Admin = () => {
  return (
    <div>
      <div className="row">
        <SiderBar />
        <MainContent />
      </div>
    </div>
  );
};

export default Admin;
