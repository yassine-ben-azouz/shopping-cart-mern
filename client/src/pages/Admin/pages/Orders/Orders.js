import React from "react";
import Loader from "../../../../components/Loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import { Fragment, useEffect } from "react";
import Table from "react-bootstrap/Table";
import { getAllOrders } from "../../../../store/Slices/OrderSlice";
import SideBar from "../../../../components/Admin/SideBar";
import DeleteOrderUser from "../../../../components/Modals/DeleteOrderUser";

const Orders = () => {
  const { ordersList, loading } = useSelector((state) => state.order);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllOrders());
  }, []);

  return (
    <div>
      <div className="row">
        <SideBar />
        <div className="col-8">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th className="table-header">count</th>
                <th className="table-header">date</th>

                <th className="table-header">username</th>
                <th className="table-header">adress</th>
                <th className="table-header">phone</th>

                <th className="table-header">amount</th>
                <th className="table-header">action</th>
              </tr>
            </thead>
            {ordersList.length ? (
              ordersList.map((order, index) => (
                <Fragment key={order._id}>
                  <tbody>
                    <tr>
                      <td className="talbe-header-count">{index + 1}</td>
                      <td className="table-content-username">
                        {order.createdAt.slice(0, 10)}
                      </td>
                      <td className="table-content-username">
                        {order.owner.username}
                        <img
                          src={order.owner.image}
                          alt={order.owner.username}
                          className="user-photo"
                        />
                      </td>
                      <td className="table-content-email">{order.adress}</td>
                      <td className="table-content-email">{order.phone}</td>

                      <td className="table-content-email">
                        {order.totalamount}$
                      </td>

                      <td className="table-action">
                        {/* Delete Conform Modal */}
                        <DeleteOrderUser id={order._id} />
                      </td>
                    </tr>
                  </tbody>
                </Fragment>
              ))
            ) : (
              <p>no orders</p>
            )}
          </Table>
        </div>
        {loading && <Loader />}
      </div>
    </div>
  );
};

export default Orders;
