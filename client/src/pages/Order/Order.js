import React, { Fragment, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../components/Loader/Loader";
import { getCartOwner } from "../../store/Slices/CartSlce";
import { addOrder } from "../../store/Slices/OrderSlice";

import "./Order.css";
const Order = () => {
  const dispatch = useDispatch();
  const [orderData, setOrderData] = useState("");
  const addNewOrder = (e) => {
    e.preventDefault();
    console.log(orderData);
    dispatch(addOrder(orderData));
  };
  const getDataInput = (e) => {
    console.log(orderData);

    setOrderData({ ...orderData, [e.target.name]: e.target.value });
  };
  useEffect(() => {
    dispatch(getCartOwner());
  }, []);
  const { cartList, loading } = useSelector((state) => state.cart);
  const totalAmount = cartList.reduce(
    (acc, cur) => acc + cur.price * cur.quantity,
    0
  );
  // totalAmount + shipping (transport)
  const totla = totalAmount + 30;
  return (
    <Fragment>
      {loading && <Loader />}
      <div className="container">
        <div className="wrapper-order">
          <div className="form-container">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">adress</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="address"
                  name="adress"
                  onChange={(e) => getDataInput(e)}
                />
                <Form.Text className="text-message "></Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">phone</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="set phone number"
                  name="phone"
                  onChange={(e) => getDataInput(e)}
                />
              </Form.Group>
              <Button
                variant=""
                type="submit"
                className="btn-submit"
                onClick={(e) => addNewOrder(e)}
              >
                Send
              </Button>
            </Form>
          </div>
          <div className="order-checkout">
            <form>
              <ul className="order-list">
                <div>
                  <li className="price-name">subtoyal</li>
                  <li className="price-count">{totalAmount}$</li>
                </div>
                <div>
                  <li className="price-name">shipping</li>
                  <li className="price-count">30$</li>
                </div>
                <hr />
                <div className="checkout-total">
                  <li className="price-name-total">total:</li>
                  <li className="price-total">{totla}$</li>
                </div>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Order;
