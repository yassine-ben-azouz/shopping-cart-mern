import { configureStore } from "@reduxjs/toolkit";
import CartSlce from "./Slices/CartSlce";
import OrderSlice from "./Slices/OrderSlice";
import ProductsSlice from "./Slices/ProductsSlice";
import UsersSlice from "./Slices/UsersSlice";

const store = configureStore({
  reducer: {
    users: UsersSlice,
    products: ProductsSlice,
    cart: CartSlce,
    order: OrderSlice,
  },
});

export default store;
