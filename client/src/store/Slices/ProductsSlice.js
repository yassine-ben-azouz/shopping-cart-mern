import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from "react-toastify";
/*==== addNewProduct =====*/
export const addNewProduct = createAsyncThunk(
  "/products/addNewProduct",
  async (data, { rejectWithValue, dispatch }) => {
    console.log("data", data);
    const form = new FormData();
    form.append("title", data.title);
    form.append("price", data.price);
    form.append("description", data.description);
    form.append("availableSizes", data.availableSizes);

    form.append("image", data.imageUpload);
    try {
      await axios.post("/api/v1/admin/products/", form, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("add product successfully ");

      return dispatch(getProducts());
    } catch (error) {
      toast.error(error.response.data.message);
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*==== addNewProduct =====*/

/*==== editProducts =====*/
export const editProducts = createAsyncThunk(
  "/products/editProduvts",
  async (data, { rejectWithValue, dispatch }) => {
    console.log("data", data);
    const form = new FormData();
    form.append("title", data.title);
    form.append("price", data.price);
    form.append("description", data.description);
    form.append("availableSizes", data.availableSizes);
    const id = data.id;

    form.append("image", data.imageUpload);
    try {
      await axios.put(`/api/v1/admin/products/${id}`, form, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("updaet product successfully ");
      return dispatch(getProducts(id));
    } catch (error) {
      toast.error(error.response.data.message);

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// editProducts //=====*/

/*==== getProducts =====*/
export const getProducts = createAsyncThunk(
  "products/getProducts",
  async (data, { rejectWithValue }) => {
    const sort = data.sort || "lastes";
    const size = data.size || "ALL";
    const title = data.title || "";
    const currentPage = data.currentPage || 1;

    try {
      const response = await axios.get(
        `/api/v1/products/?sort=${sort}&size=${size}&title=${title}&currentPage=${currentPage}`,
        {
          headers: {
            authorization: localStorage.getItem("token"),
          },
        }
      );
      return response.data;
    } catch (error) {
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// getProducts //=====*/

/*==== deleteProduct =====*/
export const deleteProduct = createAsyncThunk(
  "products/deleteProduct",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`/api/v1/admin/products/${id}`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      toast.success("product delete successfully");

      return dispatch(getProducts(id));
    } catch (error) {
      toast.error(error.response.data.message);

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// deleteProduct //=====*/

const ProductsSlice = createSlice({
  name: "products",
  initialState: {
    productsList: [],
    loading: false,
    errors: null,
    pages: 5,
    countProducts: null,
    productsLength: null,
  },

  extraReducers: {
    [getProducts.pending]: (state) => {
      state.loading = true;
    },
    [getProducts.fulfilled]: (state, action) => {
      state.errors = null;
      state.loading = false;
      state.productsList = action.payload.products;
      state.pages = action.payload.pages;
      state.countProducts = action.payload.countProducts;
      state.productsLength = action.payload.productsLength;
    },
    [getProducts.rejected]: (state, action) => {
      state.errors = true;
      state.loading = false;
    },
  },
});
export const { filterProdcutsBySize } = ProductsSlice.actions;
export default ProductsSlice.reducer;
