import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from "react-toastify";

export const getAllOrders = createAsyncThunk(
  "order/getOrder",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios.get(`/api/v1/admin/orders/`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);

export const addOrder = createAsyncThunk(
  "order/addOrder",
  async (data, { rejectWithValue }) => {
    console.log(data);

    try {
      const response = await axios.post("/api/v1/orders/", data, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      toast.success("order add successfully");
      return response.data;
    } catch (error) {
      console.log(error);
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====== getUserOrder =====*/

export const getUserOrder = createAsyncThunk(
  "order/addored",
  async (id, { rejectWithValue }) => {
    console.log(id);

    try {
      const response = await axios.get(`/api/v1/admin/orders/${id}`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      console.log(response);
      return response.data;
    } catch (error) {
      console.log(error);
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*======// getUserOrder //=====*/

/*====== deleteOrder =====*/
export const deleteOrder = createAsyncThunk(
  "order/deleteOrder",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`/api/v1/admin/orders/owner/${id}`, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("order delete successfully");

      return dispatch(getAllOrders());
    } catch (error) {
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*======// deleteOrder //=====*/

const OrderSlice = createSlice({
  name: "order",
  initialState: {
    ordersList: [],
    userOrders: [],
    looding: false,
    errors: null,
  },
  extraReducers: {
    [getAllOrders.pending]: (state, action) => {
      state.looding = true;
      state.errors = null;
    },

    [getAllOrders.fulfilled]: (state, action) => {
      console.log("action", action);
      state.looding = false;
      state.errors = null;
      state.ordersList = action.payload;
    },
    [getAllOrders.rejected]: (state, action) => {
      state.looding = false;
      state.errors = action.payload;
    },

    [getUserOrder.pending]: (state, action) => {
      state.looding = true;
      state.errors = null;
    },

    [getUserOrder.fulfilled]: (state, action) => {
      console.log("action", action);
      state.looding = false;
      state.errors = null;
      state.userOrders = action.payload;
    },
    [getUserOrder.rejected]: (state, action) => {
      state.looding = false;
      state.errors = action.payload;
    },
  },
});
export default OrderSlice.reducer;
