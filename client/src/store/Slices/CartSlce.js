import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from "react-toastify";

/*==== addInCart =====*/

export const addInCart = createAsyncThunk(
  "Cart/addInCart",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      const token = localStorage.getItem("token");

      const headers = {
        authorization: token,
      };
      await axios.post(`/api/v1/cart/${id}`, id, {
        headers,
      });
      /** 
       * @erros
       I sent the id, because the axios did not accept the method post with out data ( req.body data)  and reads headers comme req.body.data
      **/
      toast.success("product add in cart successfully");
      return dispatch(getCartOwner());
    } catch (error) {
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// addInCart //=====*/

/*==== getCartOwner =====*/

export const getCartOwner = createAsyncThunk(
  "Cart/getCartOwner",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios.get(`/api/v1/cart/ownercart`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// getCartOwner //=====*/

/*==== changeQantity =====*/

export const changeQantity = createAsyncThunk(
  "cart/changeQantity",
  async (data, { rejectWithValue, dispatch }) => {
    const { payload } = data;

    const id = data.choosenItem.product._id;

    try {
      await axios.put(
        `/api/v1/cart/${id}`,
        { payload },
        {
          headers: {
            authorization: localStorage.getItem("token"),
          },
        }
      );
      toast.success("update product successfully");

      return dispatch(getCartOwner());
    } catch (error) {
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// changeQantity //=====*/

/*==== deletePrdoductInCart =====*/

export const deletePrdoductInCart = createAsyncThunk(
  "/cart/deletePrdoductInCart",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`/api/v1/cart/deleteProductInCart/${id}`, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("delete product successfully");

      return dispatch(getCartOwner());
    } catch (error) {
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// deletePrdoductInCart //=====*/

/*==== deleteAllCartOwner =====*/

export const deleteAllCartOwner = createAsyncThunk(
  "/cart/deleteAllCartOwner",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`/api/v1/cart/`, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("remove all products in cart successfully");

      return dispatch(getCartOwner());
    } catch (error) {
      toast.error(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);
/*====// deleteAllCartOwner //=====*/
const CartSlice = createSlice({
  name: "cart",
  initialState: {
    cartList: [],
    errors: null,
    loading: false,
    toggleCart: false,
  },

  reducers: {
    toogleCartOwner: (state) => {
      state.toggleCart = !state.toggleCart;
    },
  },
  extraReducers: {
    /*==== addInCart =====*/
    [addInCart.fulfilled]: (state, action) => {
      state.errors = null;
      state.loading = false;
    },
    [addInCart.rejected]: (state, action) => {
      state.errors = action.payload;
      state.loading = false;
    },
    /*====// addInCart //=====*/

    /*==== getCartOwner =====*/
    [getCartOwner.pending]: (state, action) => {
      state.errors = null;
      state.loading = true;
    },
    [getCartOwner.fulfilled]: (state, action) => {
      state.cartList = action.payload;
      state.errors = null;
      state.loading = false;
    },
    [getCartOwner.rejected]: (state, action) => {
      state.errors = action.payload;
      state.loading = false;
    },
    /*====// getCartOwner //=====*/

    /*==== deletePrdoductInCart =====*/
    [deletePrdoductInCart.fulfilled]: (state, action) => {
      state.errors = null;
      state.loading = false;
    },
    [deletePrdoductInCart.rejected]: (state, action) => {
      state.errors = action.payload;
      state.loading = false;
    },
    /*====// deletePrdoductInCart //=====*/
  },
});
export const { toogleCartOwner } = CartSlice.actions;
export default CartSlice.reducer;
