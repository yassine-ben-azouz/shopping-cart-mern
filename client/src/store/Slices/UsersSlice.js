import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { toast } from "react-toastify";

/*==== registerUser =====*/
export const registerUser = createAsyncThunk(
  "users/registerUser",
  async (data, { rejectWithValue }) => {
    const role = data.role || "customer";
    const image = data.imageUpload || null;

    const form = new FormData();
    form.append("username", data.username);
    form.append("email", data.email);
    form.append("password", data.password);
    form.append("role", role);
    form.append("image", image);

    try {
      const res = await axios.post("/api/v1/users/register", form);
      toast.success("register user success");

      return res.data;
    } catch (error) {
      toast.error(error.response.data.message);

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
    }
  }
);
/*====// registerUser //=====*/

/*==== loginUser =====*/
export const loginUser = createAsyncThunk(
  "users/loginUser",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios.post("/api/v1/users/login", data);
      const decodedToken = jwt_decode(response.data.token);
      const token = response.data.token;
      const allResponse = { ...decodedToken, token };
      toast.success("login user success");
      return allResponse;
    } catch (error) {
      toast.error(error.response.data.message);

      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
    }
  }
);
/*====// loginUser //=====*/

/*==== getUserInfo =====*/
export const getUserInfo = createAsyncThunk(
  "users/getUserInfo",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios.get("/api/v1/users/", {
        headers: { authorization: localStorage.getItem("token") },
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      );
    }
  }
);

/*====// getUserInfo //=====*/

/*==== getAllUsers =====*/
export const getAllUsers = createAsyncThunk(
  "/users/getAllUsers",
  async (data, { rejectWithValue }) => {
    try {
      const response = await axios(`/api/v1/admin/users/`, {
        headers: {
          authorization: localStorage.getItem("token"),
        },
      });
      return response.data;
    } catch (error) {
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
);

/*====// getAllUsers //=====*/
/*==== deleteUser =====*/
export const deleteUser = createAsyncThunk(
  "users/deleteUser",
  async (id, { rejectWithValue, dispatch }) => {
    console.log("id=", id);
    try {
      await axios.delete(`/api/v1/admin/users/${id}`, {
        headers: { authorization: localStorage.getItem("token") },
      });
      toast.success("delete user success");

      return dispatch(getAllUsers());
    } catch (error) {
      toast.error(error.response.data.message);
      return rejectWithValue(
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response
      );
    }
  }
  /*====// deleteUser //=====*/
);
/*====// deleteUser //=====*/

const UserSlice = createSlice({
  name: "users",
  initialState: {
    usersList: [],
    userInfo: {},
    token: localStorage.getItem("token") || null,
    isAuth: JSON.parse(localStorage.getItem("isAuth")) || false,
    isAdmin: JSON.parse(localStorage.getItem("isAdmin")) || false,
    errors: null,
    loading: false,
    getUserError: null,
    alertShow: JSON.parse(localStorage.getItem("alertShow")) || false,
  },
  reducers: {
    /*==== logoutUser =====*/

    logoutUser: (state, action) => {
      localStorage.removeItem("token");
      localStorage.setItem("isAuth", false);
      localStorage.setItem("isAdmin", false);
      state.token = null;
      state.isAuth = false;
      state.errors = null;
      state.isAdmin = false;
    },
  },
  /*====// logoutUser //=====*/

  extraReducers: {
    /*==== registerUser =====*/

    [registerUser.fulfilled]: (state, action) => {
      state.token = action.payload.token;
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("isAuth", true);
      state.isAuth = true;
      state.errors = null;
    },
    [registerUser.rejected]: (state, action) => {
      state.errors = action.payload;
    },
    /*====// registerUser //=====*/

    /*==== login =====*/
    [loginUser.fulfilled]: (state, action) => {
      const { role } = action.payload;
      state.token = action.payload.token;
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("isAuth", true);
      state.isAuth = true;
      state.errors = null;

      if (role === "admin") {
        state.isAdmin = true;
        localStorage.setItem("isAdmin", true);
      }
    },
    [loginUser.rejected]: (state, action) => {
      state.errors = action.payload;
    },
    /*====// login //=====*/

    /*==== getUserInfo =====*/
    [getUserInfo.pending]: (state, action) => {
      state.loading = true;
    },
    [getUserInfo.fulfilled]: (state, action) => {
      state.loading = false;
      state.userInfo = action.payload;
      state.getUserError = null;
    },
    [getUserInfo.rejected]: (state, action) => {
      state.loading = false;
      state.getUserError = action.payload;
    },
    /*====// getUserInfo //=====*/

    /*==== getAllUsers =====*/

    [getAllUsers.pending]: (state, action) => {
      state.loading = true;
      state.errors = null;
    },
    [getAllUsers.fulfilled]: (state, action) => {
      state.loading = false;
      state.usersList = action.payload;
      state.errors = false;
    },
    [getAllUsers.rejected]: (state, action) => {
      state.loading = false;
      state.errors = action.payload;
    },
    /*====// getAllUsers //=====*/
  },
});

export const { logoutUser, hideAlert } = UserSlice.actions;
export default UserSlice.reducer;
