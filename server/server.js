const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();
const path = require("path");

// connect to data base
require("./config/connectDB");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors("http://localhost:3000"));

app.use(
  "/products",
  express.static(path.join(__dirname, "assets", "images", "products"))
);
app.use(
  "/users",
  express.static(path.join(__dirname, "assets", "images", "users"))
);

// users route
app.use("/api/v1/users", require("./routes/UsersRoutes"));

// products route
app.use("/api/v1/products", require("./routes/ProductsRoutes"));

// cart route
app.use("/api/v1/cart", require("./routes/CartRoutes"));

// order route
app.use("/api/v1/orders", require("./routes/OrdersRoutes"));

// order route
app.use("/api/v1/admin", require("./routes/AdminRoutes"));
// server run
app.listen(process.env.PORT, () => {
  console.log(`server is run on port ${process.env.PORT}`);
});
