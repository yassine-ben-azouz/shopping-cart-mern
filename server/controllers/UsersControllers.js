const Users = require("../models/UsersModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Order = require("../models/OrdersModel");
const { validationResult } = require("express-validator");
const Cart = require("../models/CartModel");

/**
 * @desc create new user
 * @params POST /api/v1/users/register
 * @access PUBLIC
 **/
exports.register = async (req, res) => {
  try {
    // destructuring req.body
    const { username, email, password, role } = req.body;
    // check inputs value is null or not
    if (!username || !email || !password)
      return res.status(400).json({ message: `Please add all fields` });
    // throw new Error("Please add all fields");

    // Finds the validation errors in this request and wraps them in an object with handy functions
    // validation email and password
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        message: "invalid email or password should be must than 5 characters",
      });
    }
    // check is useer already registred`
    const existUser = await Users.findOne({ email });
    if (existUser)
      return res.status(400).json({ message: `User already exists` });
    // crypt password
    let salt = bcrypt.genSaltSync(10); // alg of crypt
    let hash = bcrypt.hashSync(password, salt);

    //image path
    const imagePath = `http://localhost:5000/users/${
      req.file?.filename || "default-avatar.png"
    }`;
    // push new user to document of users in database
    const newUser = await Users.create({
      username,
      email,
      password: hash,
      role,
      image: imagePath,
    });
    // declaration of token and push if in token sign
    const token = await jwt.sign(
      { sub: newUser._id, role },
      process.env.JWT_SECRET
    );
    res.json({ token, username, email, role, image: newUser.image });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc login user
 * @params POST /api/v1/users/login
 * @access PUBLIC
 **/
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    // check inputs value is null or not
    if (!email || !password)
      return res.status(400).json({ message: `Please add all fields` });
    // throw new Error("Please add all fields");
    const existUser = await Users.findOne({ email });
    if (!existUser)
      return res.status(400).json({ message: "you shold register first" });
    // check password is correct or not
    const validate = await bcrypt.compare(password, existUser.password);

    if (!validate) return res.status(400).json({ message: "invalid password" });
    const token = jwt.sign(
      { sub: existUser._id, role: existUser.role },
      process.env.JWT_SECRET
    );
    res.json({ success: true, token });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc get All users
 * @params POST /api/v1/admin/users/
 * @access PRIVATE (onlay admin)
 **/
exports.getAllUsers = async (req, res) => {
  try {
    const AllUsers = await Users.find({}).select("-password");
    res.json(AllUsers);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc get user connect by token
 * @params POST /api/v1/admin/users/userconnect
 * @access PRIVATE owner
 **/
exports.getUserConnectInfo = async (req, res) => {
  try {
    // get user by id (req.userId from authMiddleware Middleware )
    const user = await Users.findById(req.userId).select("-password"); // select() method to filter documents select("-password") delete or select({username : 1, image :1}) get fildes

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc get user by id
 * @params POST /api/v1/admin/users/:id
 * @access PRIVATE admin
 **/
exports.getUser = async (req, res) => {
  try {
    const user = await Users.findById(req.params.id).select("-password");
    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc update user by id
 * @params PUT /api/v1/users/update
 * @access PRIVATE admin
 **/
exports.updateUser = async (req, res) => {
  try {
    const { username, password, role } = req.body;

    if (!username || !password)
      return res.status(400).json({ message: `Please add all fields` });
    let salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);
    const imagePath = `http://localhost5000/users/${
      req.file?.filename || "default-avatar.png"
    }`;
    const UpadateUser = await Users.findByIdAndUpdate(req.userId, {
      username,
      password: hash,
      role,
      image: imagePath,
    });
    res.json(UpadateUser);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc delete all users
 * @params POST /api/v1/admin/users
 * @access PRIVATE admin
 **/
exports.deleteAllUsers = async (req, res) => {
  try {
    const deleteUsers = await Users.deleteMany();
    res.json({ success: true, deleteUsers });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
/**
 * @desc delete user by id
 * @params POST /api/v1/admin/users/:id
 * @access PRIVATE
 **/
exports.deleteUser = async (req, res) => {
  console.log(req.params.id);
  if (!req.params.id)
    return res.status(400).json({ message: "id params undifend" });
  try {
    // delete all orders of user chossen
    const deleteOrders = await Order.deleteMany({ owner: req.params.id });
    // delete all carts of user chossen
    const deleteCart = await Cart.deleteMany({ owner: req.params.id });

    const deleteUser = await Users.findByIdAndDelete(req.params.id).select(
      "-passwoed"
    );
    res.json({ success: true, deleteUser });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "something wrong" });
  }
};
