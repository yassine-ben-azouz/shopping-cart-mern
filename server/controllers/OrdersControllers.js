const Orders = require("../models/OrdersModel");
const Cart = require("../models/CartModel");

/**
 * @desc add order in orders
 * @params POST /api/v1/orders/
 * @access PRIVATE owner (user connect)
 **/
exports.newOrder = async (req, res) => {
  try {
    const { adress, phone } = req.body;
    if (!adress || !phone)
      return res.status(400).json({ message: "fill all fildes" });
    const cartOwner = await Cart.find({ owner: req.userId });
    if (!cartOwner.length)
      return res.status(400).json({ message: "you dont have a order" });
    const totalamount = cartOwner.reduce(
      (acc, cur) => acc + cur.quantity * cur.price,
      0
    );
    const totla = totalamount + 30;

    const newOrder = await Orders.create({
      owner: req.userId,
      totalamount,
      adress,
      phone,
      totla,
    });
    res.status(200).json(newOrder);
  } catch (error) {
    res.status(500).json(error);
  }
};

/**
 * @desc get All orders
 * @params GET /api/v1/admin/orders/
 * @access PRIVATE admin
 **/

exports.getAllOrders = async (req, res) => {
  try {
    const orders = await Orders.find().populate(
      "owner",
      "username email image"
    );

    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @desc get All users orders
 * @params GET /api/v1/admin/orders/:id
 * @access PRIVATE admin
 **/

exports.getUserOrder = async (req, res) => {
  try {
    if (!req.params.id)
      return res.status(400).json({ message: "id params is undifend" });
    const orders = await Orders.findOne({ owner: req.params.id }).populate(
      "owner",
      "username email"
    );
    if (!orders) return res.status(400).json({ message: "invalid id params" });
    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @desc update order in orders
 * @params PUT /api/v1/cart/orders/
 * @access PRIVATE owner (user connect)
 **/
exports.updateOrder = async (req, res) => {
  try {
    const cartOwner = await Cart.find({ owner: req.userId });
    if (!cartOwner.length)
      return res.status(400).json({ message: "you dont have a cart" });
    const existOrder = await Orders.findOne({ owner: req.userId });
    if (!existOrder)
      return res.status(400).json({ message: "you dont have a order" });
    const totalamount = cartOwner.reduce(
      (acc, cur) => acc + cur.quantity * cur.price,
      0
    );

    const updateOrder = await Orders.findByIdAndUpdate(existOrder._id, {
      owner: req.userId,
      totalamount,
    });
    console.log("updateOrder", updateOrder);
    res.status(200).json({ success: true });
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @desc delete All  orders
 * @params DELETE /api/v1/admin/orders/
 * @access PRIVATE admin
 **/
exports.deleteAllOrders = async (req, res) => {
  try {
    const deleteAll = await Orders.deleteMany();
    res.status(200).json({ success: true, deleteAll });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
  }
};

/**
 * @desc delete selecte order user (one)
 * @params DELETE /api/v1/admin/orders/owner/:id
 * @access PRIVATE owner
 **/
exports.deleteUserOrder = async (req, res) => {
  try {
    const existOrder = await Orders.findOne({ owner: req.userId });
    if (!existOrder)
      return res.status(400).json({ message: "user d'ont have a order" });
    const deleteOrder = await Orders.findByIdAndDelete(req.params.id);
    res.status(200).json({ success: true, deleteOrder });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
  }
};

/**
 * @desc delete All User  orders
 * @params DELETE /api/v1/admin/orders/owner/All
 * @access PRIVATE owner
 **/
exports.deleteAllUserOrder = async (req, res) => {
  try {
    const existOrder = await Orders.findOne({ owner: req.userId });
    if (!existOrder)
      return res.status(400).json({ message: "user d'ont have a order" });
    const deleteAll = await Orders.deleteMany({ owner: req.userId });
    res.status(200).json({ success: true, deleteAll });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
  }
};
