const Cart = require("../models/CartModel");
const Products = require("../models/ProductsModel");

/**
 * @desc add product in cart
 * @params POST /api/v1/cart/:id
 * @access PRIVATE owner (user connect)
 **/
exports.addProductInCart = async (req, res) => {
  try {
    // get productsChoosen by id
    const productChoosen = await Products.findById(req.params.id);
    if (!productChoosen)
      return res.status(400).json({ message: "product not in list" });
    const existProduct = await Cart.find({
      owner: req.userId, // get owenr (usre connected) cart
      product: productChoosen._id, // check product in cart or note
    });
    // exist product true (product in cart) , exist.length because return empty array [] and is true
    if (existProduct.length) {
      res.status(400).json({ message: "product already in cart" });
    } else {
      let quantity = 1;
      const products = await Cart.create({
        product: productChoosen._id, // id product or req.params.id to populate then get cart
        owner: req.userId, // id user connect, to populate then get cart
        quantity,

        price: productChoosen.price,
      });
      res.json(products);
    }
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};
/**
 * @desc get all carts
 * @params GET /api/v1/cart/
 * @access PRIVATE admin
 **/
exports.getAllCarts = async (req, res) => {
  try {
    const CartProducts = await Cart.find()

      .populate("owner", "username")
      .populate("product");

    if (!CartProducts.length)
      return res.status(401).json({ message: "cart is empty" });
    res.json(CartProducts);
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @desc get user products  from cart
 * @params GET /api/v1/cart/
 * @access PRIVATE owner (user connect)
 **/
exports.getCartUserProducts = async (req, res) => {
  try {
    const CartProducts = await Cart.find({ owner: req.userId })

      .populate("owner", "username image")
      .populate("product");

    res.json(CartProducts);
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @desc get user products  from cart
 * @params GET /api/v1/cart/
 * @access PRIVATE admin admin
 **/
exports.getCartUserById = async (req, res) => {
  try {
    if (!req.params.id)
      return res.status(400).json({ message: "id params is undifend" });
    const CartProducts = await Cart.find({ owner: req.params.id })

      .populate("owner", "username")
      .populate("product");

    if (!CartProducts.length)
      return res.status(401).json({ message: "cart is empty" });
    res.json(CartProducts);
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};
/**
 * @desc update cart
 * @params PUT /api/v1/cart/:id
 * @access PRIVATE owner (user connect)
 **/
exports.updateUserCart = async (req, res) => {
  try {
    const { payload } = req.body;
    //check product in product list or not
    const productChoosen = await Products.findById(req.params.id);
    if (!productChoosen)
      return res
        .status(400)
        .json({ message: "product not in list of products" });

    //check the identity of the owner of the cart
    const existkOwner = await Cart.findOne({ owner: req.userId });

    if (!existkOwner) return res.status(400).json({ message: "cart is empty" });

    // check ower cart for change qunatity of product
    const productOwnerInCartChoosen = await Cart.find({
      owner: req.userId,
      product: req.params.id,
    });

    //check product in product list or not
    if (!productOwnerInCartChoosen.length)
      return res.status(401).json({
        message:
          "product not in owner cart and the cart is owned by someone else",
      });

    const quantity = productOwnerInCartChoosen[0].quantity + parseInt(payload);
    if (!quantity) {
      return res
        .status(400)
        .json({ message: "quantity of product some be great then 1" });
    }
    // get cart owener connected
    const cartChoosen = await Cart.updateOne(
      { owner: req.userId, product: req.params.id },
      {
        owner: req.userId, // id user connect, to populate then get cart
        quantity,
      }
    );
    res.json({ success: true, cartChoosen });
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};
/**
 * @desc delete product by id from cart
 * @params DELETE /api/v1/cart/deleteProductInCart/:id
 * @access PRIVATE owner (user connect)
 **/
exports.deleteProductInCart = async (req, res) => {
  try {
    // check product in products list or not
    const existProduct = await Products.findById(req.params.id);
    if (!existProduct)
      return res
        .status(400)
        .json({ message: "product not in list of products" });
    // check owner is have cart or cart is emty
    const existkOwner = await Cart.findOne({ owner: req.userId });
    if (!existkOwner) return res.status(400).json({ message: "cart is empty" });
    // check product choosen is in  cart or not
    const existProductInCart = await Cart.find({
      product: req.params.id,
      owner: req.userId,
    });
    if (!existProductInCart.length)
      return res.status(400).json({ message: "product not in cart" });
    const deletePrdoduct = await Cart.deleteOne({
      owner: req.userId,
      product: req.params.id,
    });
    res.status(200).json({ success: true, deletePrdoduct });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
};

/**
 * @desc delete all products in cart user
 * @params DELETE /api/v1/cart/
 * @access PRIVATE owner (user connect)
 **/
exports.deleteAllProductsInCart = async (req, res) => {
  try {
    // check
    const existOwner = await Cart.findOne({ owner: req.userId });
    if (!existOwner)
      return res
        .status(401)
        .json({ message: "you are not authorized and you cart is empty" });
    const deleteAllProducts = await Cart.deleteMany({ owner: req.userId });
    res.status(200).json({ success: true, deleteAllProducts });
  } catch (error) {
    res.status(500).json("something wrong");
    throw new Error(error);
  }
};

/**
 * @desc delete all cart
 * @params DELETE /api/v1/admin/cart/
 * @access PRIVATE admin
 **/

exports.deleteAllCarts = async (req, res) => {
  try {
    const deleteCarts = await Cart.deleteMany();
    res.json({ success: true, deleteCarts });
  } catch (error) {
    res.status(500).json("something wrong");
    throw new Error(error);
  }
};
