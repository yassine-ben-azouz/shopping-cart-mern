const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true, // check is email alrady exist
    },
    password: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },

    role: {
      type: String,
      enum: ["admin", "customer"], // enum: role adimn or customer else error
      default: "customer",
    },
  },
  { timestamps: true } // create tima and update time
);

module.exports = mongoose.model("user", userSchema);
