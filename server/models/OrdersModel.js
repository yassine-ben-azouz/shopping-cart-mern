const mongoose = require("mongoose");

const orderSchema = mongoose.Schema(
  {
    // cart: {
    //   type: mongoose.Types.ObjectId,
    //   ref: "cart",
    //   required: true,
    // },
    owner: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "user",
    },
    adress: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },

    totalamount: {
      type: Number,
      required: true,
    },
    totla: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("order", orderSchema);
