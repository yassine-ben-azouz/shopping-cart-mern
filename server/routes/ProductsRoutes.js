const {
  addnewproduct,
  getAllProducts,
  deleteAllProducts,
  getPrductById,
  deleteProductById,
  updateProduct,
} = require("../controllers/ProductsControllers");
const multer = require("multer");
const { authMiddleware } = require("../middlewares/authMiddlewares");
const { isAdmin } = require("../middlewares/isAdmin");

const router = require("express").Router();

/*============ Multer ==========*/
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "server/assets/images/products");
  },
  filename: function (req, file, cb) {
    const imageName = Date.now() + "-" + file.originalname;
    cb(null, imageName);
  },
});

const upload = multer({ storage: storage });

/*============// Multer //==========*/

// add new product
router.post(
  "/addnewproduct",
  authMiddleware,
  upload.single("image"),
  addnewproduct
);
// get  all products
router.get("/", getAllProducts);
// get product by id
router.get("/:id", getPrductById);
// update product by id
router.put(
  "/update/:id",
  authMiddleware,
  upload.single("image"),
  updateProduct
);
// delete product by id
router.delete("/delete/:id", deleteProductById);
// delete all products
router.delete("/deleteAll", deleteAllProducts);

module.exports = router;
