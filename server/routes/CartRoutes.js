const {
  addProductInCart,
  getCartUserProducts,
  updateUserCart,
  deleteAllProductsInCart,
  deleteAllCarts,
  deleteProductInCart,
  getAllCarts,
} = require("../controllers/CartControllers");

const { authMiddleware } = require("../middlewares/authMiddlewares");
const { isAdmin } = require("../middlewares/isAdmin");

const router = require("express").Router();
// add new product by id in cart

// get cart owner
router.post("/:id", authMiddleware, addProductInCart);
router.get("/ownercart", authMiddleware, getCartUserProducts);
router.get("/", authMiddleware, getAllCarts);

router.put("/:id", authMiddleware, updateUserCart);

router.delete("/", authMiddleware, deleteAllProductsInCart);
router.delete("/deleteAllCarts/", authMiddleware, isAdmin, deleteAllCarts);
router.delete("/deleteProductInCart/:id", authMiddleware, deleteProductInCart);

module.exports = router;
