const multer = require("multer");
const router = require("express").Router();
const {
  getAllCarts,
  deleteAllCarts,
  getCartUserById,
} = require("../controllers/CartControllers");
const {
  getAllOrders,
  deleteAllOrders,
  deleteUserOrder,
  getUserOrder,
  deleteAllUserOrder,
} = require("../controllers/OrdersControllers");
const {
  getAllProducts,
  getPrductById,
  addnewproduct,
  updateProduct,
  deleteAllProducts,
  deleteProductById,
} = require("../controllers/ProductsControllers");
const {
  getAllUsers,
  getUser,
  deleteAllUsers,
  deleteUser,
  register,
  login,
} = require("../controllers/UsersControllers");

const { authMiddleware } = require("../middlewares/authMiddlewares");
const { isAdmin } = require("../middlewares/isAdmin");

/*============ Multer ==========*/

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "server/assets/images/users");
  },
  filename: (req, file, cb) => {
    const image_name = Date.now() + "-" + file.originalname;
    cb(null, image_name);
  },
});

const upload = multer({ storage: storage });

/*============// Multer //==========*/

/*===== register and login =====*/
router.post("/register", upload.single("image"), register);
router.post("/login", login);
/*===== register and login =====*/

/*===== users =====*/
router.get("/users", authMiddleware, isAdmin, getAllUsers);
router.get("/users/:id", authMiddleware, isAdmin, getUser);
router.delete("/users", authMiddleware, isAdmin, deleteAllUsers);
router.delete("/users/:id", authMiddleware, isAdmin, deleteUser);
/*=====// users //=====*/

/*===== products =====*/
router.post(
  "/products/",
  authMiddleware,
  isAdmin,
  upload.single("image"),
  addnewproduct
);

router.get("/products", authMiddleware, isAdmin, getAllProducts);
router.get("/products/:id", authMiddleware, isAdmin, getPrductById);

router.put(
  "/products/:id",
  authMiddleware,
  isAdmin,
  upload.single("image"),
  updateProduct
);

router.delete("/products/", authMiddleware, isAdmin, deleteAllProducts);
router.delete("/products/:id", authMiddleware, isAdmin, deleteProductById);
/*=====// products //=====*/

/*===== cart =====*/
router.get("/carts/", authMiddleware, isAdmin, getAllCarts);
router.get("/carts/:id", authMiddleware, isAdmin, getCartUserById);
router.delete("/carts/", authMiddleware, isAdmin, deleteAllCarts);
/*=====// cart //=====*/

/*===== order =====*/
router.get("/orders/", authMiddleware, isAdmin, getAllOrders);
router.get("/orders/user/:id", authMiddleware, isAdmin, getUserOrder);

// delte all orders
router.delete("/orders", authMiddleware, isAdmin, deleteAllOrders);
// delte owner oreder
router.delete("/orders/owner/all", authMiddleware, deleteAllUserOrder);
// delte owner oreder
router.delete("/orders/owner/:id", authMiddleware, deleteUserOrder);

/*=====// order //=====*/

module.exports = router;
