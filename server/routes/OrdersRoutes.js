const {
  newOrder,

  updateOrder,
} = require("../controllers/OrdersControllers");
const { authMiddleware } = require("../middlewares/authMiddlewares");
const { isAdmin } = require("../middlewares/isAdmin");

const router = require("express").Router();

// add owenr order
router.post("/", authMiddleware, newOrder);
// update owner order
router.put("/", authMiddleware, isAdmin, updateOrder);

module.exports = router;
