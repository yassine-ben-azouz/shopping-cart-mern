const router = require("express").Router();
const { body } = require("express-validator");
const multer = require("multer");
const {
  register,
  login,
  getAllUsers,
  getUser,
  deleteAllUsers,
  deleteUser,
  getUserConnectInfo,
  updateUser,
} = require("../controllers/UsersControllers");
const { authMiddleware } = require("../middlewares/authMiddlewares");
const { isAdmin } = require("../middlewares/isAdmin");
/*============ Multer ==========*/

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "server/assets/images/users");
  },
  filename: (req, file, cb) => {
    const image_name = Date.now() + "-" + file.originalname;
    cb(null, image_name);
  },
});

const upload = multer({ storage: storage });

/*============// Multer //==========*/

// register new user
router.post(
  "/register",

  upload.single("image"),
  body("email").isEmail(),
  body("password").isLength({ min: 5 }),
  // upload.array("image", 3),
  register
);
// login
router.post("/login", login);

// get  user by token
router.get("/", authMiddleware, getUserConnectInfo);

// update user by id
router.put("/", authMiddleware, upload.single("image"), updateUser);

module.exports = router;
